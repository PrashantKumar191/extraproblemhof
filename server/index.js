const fs = require('fs');
const path = require('path');


const problemFunction = require('./problem')

const filePath = path.resolve(`${__dirname}`,'../jsonDataFile.json')

const practiceData = function(inputFunction){
    fs.readFile(filePath,'utf-8',(err,data)=>{
        if(err){
            console.log(err);
        }
        else{
            jsonData = JSON.parse(data);
            let result = inputFunction(jsonData);
            outputFile  = JSON.stringify(result.file,null,2);
            outputFilePath = path.resolve(`${__dirname}`,`../output/${result.filename}.json`);
        }
        fs.writeFile(outputFilePath,outputFile,(err)=>{
            if(err){
                console.log(err);
            }
            else{
                console.log("saved");
            }
        })
    })
}





practiceData(problemFunction.webDevelopersOnly);
practiceData(problemFunction.salaryInNumber);
practiceData(problemFunction.salaryFactorOf10000);
practiceData(problemFunction.sumSalary);
practiceData(problemFunction.salaryCountryWise);
practiceData(problemFunction.averageSalary);
